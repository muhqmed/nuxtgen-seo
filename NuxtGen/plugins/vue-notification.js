import Vue from "vue";
import Notifications from "vue-notification";

Vue.use(Notifications, {
  position: "bottom right",
  ignoreDuplicates: true
});
