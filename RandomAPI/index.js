import Fastify from "fastify";
import Product from "./product.js";
import fastifyCors from "fastify-cors";

const frontURLhttps = "https://stale-moose-95.loca.lt";
const frontURLhttp = "http://stale-moose-95.loca.lt";
const frontURLwww = "www.https://stale-moose-95.loca.lt"; //"http://localhost:3000";
const fastify = Fastify({ logger: true });
fastify.register(fastifyCors, {
  origin: ["https://curvy-lionfish-24.loca.lt"]
});

fastify.get("/", async (req, res) => Product.getProduct());

fastify.get("/product/:id", async (req, res) =>
  Product.getProductById(req.params.id)
);

fastify.listen(4000, (err, address) => {
  if (err) throw err;
  fastify.log.info(`server listening on ${address}`);
});
