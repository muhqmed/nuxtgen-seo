const product = [
  {
    id: 1,
    name: "Magnetic Charger",
    type: "Charger",
    niche: "Electronics",
    Avalaible: true
  },
  {
    id: 2,
    name: "Auto Charger",
    type: "Charger",
    niche: "Electronics",
    Avalaible: true
  },
  {
    id: 3,
    name: "Desk Charger",
    type: "Charger",
    niche: "Electronics",
    Avalaible: true
  },
  {
    id: 4,
    name: "Drugs Charger",
    type: "Charger",
    niche: "Electronics",
    Avalaible: false
  },
  {
    id: 5,
    name: "Other Charger",
    type: "Charger",
    niche: "Electronics",
    Avalaible: false
  },
  {
    id: 6,
    name: "MacBook Charger",
    type: "Charger",
    niche: "Electronics",
    Avalaible: true
  },
  {
    id: 7,
    name: "Samsung Galaxy S21",
    type: "Smartphone",
    niche: "Mobile",
    Avalaible: true
  },
  {
    id: 8,
    name: "Apple iPhone 12 Pro Max",
    type: "Smartphone",
    niche: "Mobile",
    Avalaible: true
  }
];

export default {
  getProduct: () => {
    return product;
  },

  getProductById: async id => {
    return product.find(product => product.id == id);
  }
};
